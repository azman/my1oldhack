#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_GOLD 30000
#define MAX_MANA 30000
#define SET_FAME 10000
#define SET_CAST 10000
#define SET_POP1 21
#define SET_LVL1 5
/* {0-outpost,1-hamlet,2-village,3-town,4-city,5-capital} */

/* main indices */
#define OFFSET_GOLD 0x0d3e
#define OFFSET_MANA 0x0c44
#define OFFSET_TURN 0x09e4
#define OFFSET_POP1 0x8ac0
#define OFFSET_WIZZ_INIT 0x09e8
/* 6 wizard slots? 1 + 4 + 1 => total = 6x1224 = 7334 (0x1cb0) */
#define OFFSET_WIZZ_ENDS 0x2698
#define OFFSET_ITEM_INIT 0x6fb8
/* item len 0x32 */
#define OFFSET_CITY_INIT 0x8aac
/* total city = 0xb734-0x8aac = 0x2c88 (11400) */
/* count city = 11400/114 = 100 */
#define CITY_COUNT_MAX 100
#define OFFSET_UNIT_INIT 0xb734
/* total unit = 0x1E1A4-0xb734 = 0x12a70 (76400) */
/* count unit = 76384 = 2387 x 32 */
#define UNIT_COUNT_MAX 1000

typedef unsigned int adwrd;
typedef unsigned short aword;
typedef unsigned char abyte;

#define ABILITY_ALCHEMY 0
#define ABILITY_WARLORD 1
#define ABILITY_CHAOS_MASTERY 2
#define ABILITY_NATURE_MASTERY 3
#define ABILITY_SORCERY_MASTERY 4
#define ABILITY_INFERNAL_POWER 5
#define ABILITY_DIVINE_POWER 6
#define ABILITY_SAGE_MASTER 7
#define ABILITY_CHANNELLER 8
#define ABILITY_MYRRAN_ 9
#define ABILITY_ARCHMAGE 10
#define ABILITY_MANA_FOCUSING 11
#define ABILITY_NODE_MASTERY 12
#define ABILITY_FAMOUS 13
#define ABILITY_RUNEMASTER 14
#define ABILITY_CONJURER 15
#define ABILITY_CHARISMATIC 16
#define ABILITY_ARTIFICIER 17

#define SPELL_UNKNOWN 0
#define SPELL_CAN_BE_LEARNED 1
#define SPELL_LEARNED 2
#define SPELL_KNOWN SPELL_LEARNED

#define SPELL_NATURE_EarthToMud 0
#define SPELL_NATURE_ResistElements 1
#define SPELL_NATURE_WallOfStone 2
#define SPELL_NATURE_GiantStrength 3
#define SPELL_NATURE_Web 4
#define SPELL_NATURE_WarBears 5
#define SPELL_NATURE_StoneSkin 6
#define SPELL_NATURE_WaterWalking 7
#define SPELL_NATURE_Sprites 8
#define SPELL_NATURE_EarthLore 9
#define SPELL_NATURE_CracksCall 10
#define SPELL_NATURE_NaturesEye 11
#define SPELL_NATURE_IceBolt 12
#define SPELL_NATURE_GiantSpiders 13
#define SPELL_NATURE_ChangeTerrain 14
#define SPELL_NATURE_Pathfinding 15
#define SPELL_NATURE_Cockatrices 16
#define SPELL_NATURE_Transmute 17
#define SPELL_NATURE_NaturesCures 18
#define SPELL_NATURE_Basilisk 19
#define SPELL_NATURE_ElementalArmor 20
#define SPELL_NATURE_Petrify 21
#define SPELL_NATURE_StoneGiant 22
#define SPELL_NATURE_IronSkin 23
#define SPELL_NATURE_IceStorm 24
#define SPELL_NATURE_Earthquake 25
#define SPELL_NATURE_Gorgons 26
#define SPELL_NATURE_MoveFortress 27
#define SPELL_NATURE_GaiasBlessing 28
#define SPELL_NATURE_EarthElemental 29
#define SPELL_NATURE_Regeneration 30
#define SPELL_NATURE_Behemoth 31
#define SPELL_NATURE_Entangle 32
#define SPELL_NATURE_NaturesAwareness 33
#define SPELL_NATURE_CallLighting 34
#define SPELL_NATURE_Colossus 35
#define SPELL_NATURE_EarthGate 36
#define SPELL_NATURE_HerbMastery 37
#define SPELL_NATURE_GreatWyrm 38
#define SPELL_NATURE_NaturesWrath 39
#define SPELL_SORCERY_ResistMagic 0
#define SPELL_SORCERY_DispelMagicTrue 1
#define SPELL_SORCERY_FloatingIsland 2
#define SPELL_SORCERY_GuardianWind 3
#define SPELL_SORCERY_PhantomWarriors 4
#define SPELL_SORCERY_Confusion 5
#define SPELL_SORCERY_WordOfRecall 6
#define SPELL_SORCERY_CounterMagic 7
#define SPELL_SORCERY_Nagas 8
#define SPELL_SORCERY_PsionicBlast 9
#define SPELL_SORCERY_Blur 10
#define SPELL_SORCERY_DisenchantTrue 11
#define SPELL_SORCERY_Vertigo 12
#define SPELL_SORCERY_SpellLock 13
#define SPELL_SORCERY_EnchantRoad 14
#define SPELL_SORCERY_Flight 15
#define SPELL_SORCERY_WindMastery 16
#define SPELL_SORCERY_SpellBlast 17
#define SPELL_SORCERY_AuraOfMastery 18
#define SPELL_SORCERY_PhantomBeast 19
#define SPELL_SORCERY_DisjunctionTrue 20
#define SPELL_SORCERY_Invisibility 21
#define SPELL_SORCERY_WindWalking 22
#define SPELL_SORCERY_Banish 23
#define SPELL_SORCERY_StormGiant 24
#define SPELL_SORCERY_AirElemental 25
#define SPELL_SORCERY_MindStorm 26
#define SPELL_SORCERY_Stasis 27
#define SPELL_SORCERY_MagicImmunity 28
#define SPELL_SORCERY_Haste 29
#define SPELL_SORCERY_Djinn 30
#define SPELL_SORCERY_SpellWard 31
#define SPELL_SORCERY_CreatureBinding 32
#define SPELL_SORCERY_MassInvisibility 33
#define SPELL_SORCERY_GreatUnSummoning 34
#define SPELL_SORCERY_SpellBinding 35
#define SPELL_SORCERY_FlyingFortress 36
#define SPELL_SORCERY_SkyDrake 37
#define SPELL_SORCERY_SuppressMagic 38
#define SPELL_SORCERY_TimeStop 39
#define SPELL_CHAOS_WarpWood 0
#define SPELL_CHAOS_Disrupt 1
#define SPELL_CHAOS_FireBolt 2
#define SPELL_CHAOS_HellHounds 3
#define SPELL_CHAOS_Corruption 4
#define SPELL_CHAOS_EldritchWeapon 5
#define SPELL_CHAOS_WallOfFire 6
#define SPELL_CHAOS_Shatter 7
#define SPELL_CHAOS_WarpCreature 8
#define SPELL_CHAOS_FireElemental 9
#define SPELL_CHAOS_LightningBolt 10
#define SPELL_CHAOS_FireGiant 11
#define SPELL_CHAOS_ChaosChannels 12
#define SPELL_CHAOS_FlameBlade 13
#define SPELL_CHAOS_Gargoyles 14
#define SPELL_CHAOS_Fireball 15
#define SPELL_CHAOS_Doombat 16
#define SPELL_CHAOS_RaiseVolcano 17
#define SPELL_CHAOS_Immolation 18
#define SPELL_CHAOS_Chimeras 19
#define SPELL_CHAOS_WarpLightning 20
#define SPELL_CHAOS_MetalFires 21
#define SPELL_CHAOS_ChaosSpawn 22
#define SPELL_CHAOS_DoomBolt 23
#define SPELL_CHAOS_MagicVortex 24
#define SPELL_CHAOS_Efreet 25
#define SPELL_CHAOS_FireStorm 26
#define SPELL_CHAOS_WarpReality 27
#define SPELL_CHAOS_FlameStrike 28
#define SPELL_CHAOS_ChaosRift 29
#define SPELL_CHAOS_Hydra 30
#define SPELL_CHAOS_Disintegrate 31
#define SPELL_CHAOS_MeteorStorm 32
#define SPELL_CHAOS_GreatWasting 33
#define SPELL_CHAOS_CallChaos 34
#define SPELL_CHAOS_ChaosSurge 35
#define SPELL_CHAOS_DoomMastery 36
#define SPELL_CHAOS_GreatDrake 37
#define SPELL_CHAOS_CallTheVoid 38
#define SPELL_CHAOS_Armageddon 39
#define SPELL_LIFE_Bless 0
#define SPELL_LIFE_StarFires 1
#define SPELL_LIFE_Endurance 2
#define SPELL_LIFE_HolyWeapon 3
#define SPELL_LIFE_Healing 4
#define SPELL_LIFE_HolyArmor 5
#define SPELL_LIFE_JustCause 6
#define SPELL_LIFE_TrueLight 7
#define SPELL_LIFE_GuardianSpirit 8
#define SPELL_LIFE_Heroism 9
#define SPELL_LIFE_TrueSight 10
#define SPELL_LIFE_PlaneShift 11
#define SPELL_LIFE_Resurrection 12
#define SPELL_LIFE_DispelEvil 13
#define SPELL_LIFE_PlanarSeal 14
#define SPELL_LIFE_Unicorns 15
#define SPELL_LIFE_RaiseDead 16
#define SPELL_LIFE_PlanarTravel 17
#define SPELL_LIFE_HeavenlyLight 18
#define SPELL_LIFE_Prayer 19
#define SPELL_LIFE_Lionhearted 20
#define SPELL_LIFE_Incarnation 21
#define SPELL_LIFE_Invulnerability 22
#define SPELL_LIFE_Righteousness 23
#define SPELL_LIFE_Prosperity 24
#define SPELL_LIFE_AlterOfBattle 25
#define SPELL_LIFE_Angel 26
#define SPELL_LIFE_StreamOfLife 27
#define SPELL_LIFE_MassHealing 28
#define SPELL_LIFE_HolyWord 29
#define SPELL_LIFE_HighPrayer 30
#define SPELL_LIFE_Inspirations 31
#define SPELL_LIFE_AstralGate 32
#define SPELL_LIFE_HolyArms 33
#define SPELL_LIFE_Consecration 34
#define SPELL_LIFE_LifeForce 35
#define SPELL_LIFE_Tranquility 36
#define SPELL_LIFE_Crusade 37
#define SPELL_LIFE_ArchAngel 38
#define SPELL_LIFE_CharmOfLife 39
#define SPELL_DEATH_Skeletons 0
#define SPELL_DEATH_Weakness 1
#define SPELL_DEATH_DarkRituals 2
#define SPELL_DEATH_CloakOfFear 3
#define SPELL_DEATH_BlackSleep 4
#define SPELL_DEATH_Ghouls 5
#define SPELL_DEATH_LifeDrain 6
#define SPELL_DEATH_Terror 7
#define SPELL_DEATH_Darkness 8
#define SPELL_DEATH_ManaLeak 9
#define SPELL_DEATH_DrainPower 10
#define SPELL_DEATH_Possession 11
#define SPELL_DEATH_Lycanthropy 12
#define SPELL_DEATH_BlackPrayer 13
#define SPELL_DEATH_BlackChannels 14
#define SPELL_DEATH_NightStalker 15
#define SPELL_DEATH_Subversion 16
#define SPELL_DEATH_WallOfDarkness 17
#define SPELL_DEATH_Berserk 18
#define SPELL_DEATH_ShadowDemons 19
#define SPELL_DEATH_WraithForm 20
#define SPELL_DEATH_Wrack 21
#define SPELL_DEATH_EvilPresence 22
#define SPELL_DEATH_Wraiths 23
#define SPELL_DEATH_CloudOfShadow 24
#define SPELL_DEATH_WarpNode 25
#define SPELL_DEATH_BlackWind 26
#define SPELL_DEATH_ZombieMastery 27
#define SPELL_DEATH_Famine 28
#define SPELL_DEATH_CursedLands 29
#define SPELL_DEATH_CruelUnminding 30
#define SPELL_DEATH_WordOfDeath 31
#define SPELL_DEATH_DeathKnights 32
#define SPELL_DEATH_DeathSpell 33
#define SPELL_DEATH_AnimateDead 34
#define SPELL_DEATH_Pestilence 35
#define SPELL_DEATH_EternalNight 36
#define SPELL_DEATH_EvilOmens 37
#define SPELL_DEATH_DeathWish 38
#define SPELL_DEATH_DemonLord 39
#define SPELL_ARCANE_MagicSpirit 0
#define SPELL_ARCANE_DispelMagic 1
#define SPELL_ARCANE_SummoningCircle 2
#define SPELL_ARCANE_DisenchantArea 3
#define SPELL_ARCANE_RecallHero 4
#define SPELL_ARCANE_DetectMagic 5
#define SPELL_ARCANE_EnchantItem 6
#define SPELL_ARCANE_SummonHero 7
#define SPELL_ARCANE_Awareness 8
#define SPELL_ARCANE_Disjunction 9
#define SPELL_ARCANE_CreateArtifact 10
#define SPELL_ARCANE_SummonChampion 11
#define SPELL_ARCANE_SpellOfMastery 12
#define SPELL_ARCANE_SpellOfReturn 13

#define WIZZ_DATA_SIZE 1224
typedef struct _wizz
{
	abyte pic; /* jafar(04) tauron(08) */
	char name[10];
	abyte unknown0[11]; /* 22 bytes */
	abyte flagcolor;
	abyte unknown1[13]; /* 36 bytes */
	aword fame;
	abyte unknown2[46]; /* 84 bytes */
	aword casting_skill_adjusted;
	aword casting_skill_personal;
	abyte unknown3[2]; /* 90 bytes */
	aword book_nature;
	aword book_sorcery;
	aword book_chaos;
	aword book_life;
	aword book_death; /* 100 bytes */
	abyte abilities[18]; /* zero,non-zero byte logic */
	abyte heroes[6][28]; /* 286 bytes */
	abyte unknown4[318]; /* 604 bytes */
	aword mana;
	aword casting_skill_personal2_; /* actually 3 bytes? */
	abyte unknown5[4]; /* 612 bytes */
	abyte spells_nature[40];
	abyte spells_sorcery[40];
	abyte spells_chaos[40];
	abyte spells_life[40];
	abyte spells_death[40];
	abyte spells_arcane[40]; /* 852 bytes */
	abyte unknown6[2]; /* 854 bytes */
	aword gold;
	aword casting_skill_adjusted2_; /* actually 3 bytes? */
	abyte unknown[366]; /* 1224 bytes */
}
wizz;

#define HERO_DATA_SIZE 28
typedef struct _hero
{
	aword offset; /* link to unit? */
	char name[14]; /* null terminated */
	aword item1, item2, item3;
	aword item1_t, item2_t, item3_t;
}
hero;

#define BUILD_BARRACKS 0
#define BUILD_ARMORY 1
#define BUILD_FIGHTERS_GUILD 2
#define BUILD_ARMORERS_GUILD 3
#define BUILD_WAR_COLLEGE 4
#define BUILD_SMITHY 5
#define BUILD_STABLES 6
#define BUILD_ANIMIST_GUILD 7
#define BUILD_FANTASTIC_STABLE 8
#define BUILD_SHIP_WRIGHTS_GUILD 9
#define BUILD_SHIP_YARD 10
#define BUILD_MARITIME_GUILD 11
#define BUILD_SAWMILL 12
#define BUILD_LIBRARY 13
#define BUILD_SAGES_GUILD 14
#define BUILD_ORACLE 15
#define BUILD_ALCHEMISTS_GUILD 16
#define BUILD_UNIVERSITY 17
#define BUILD_WIZARDS_GUILD 18
#define BUILD_SHRINE 19
#define BUILD_TEMPLE 20
#define BUILD_PARTHENON 21
#define BUILD_CATHEDRAL 22
#define BUILD_MARKETPLACE 23
#define BUILD_BANK 24
#define BUILD_MERCHANTS_GUILD 25
#define BUILD_GRANARY 26
#define BUILD_FARMERS_MARKET 27
#define BUILD_FORESTERS_GUILD 28
#define BUILD_BUILDERS_HALL 29
#define BUILD_MECHANICIAN_GUILD 30
#define BUILD_MINERS_GUILD 31
#define BUILD_CITY_WALLS 32

#define RACE_BARBARIAN 0
#define RACE_BEASTMAN 1
#define RACE_DARK_ELF 2
#define RACE_DRACONIAN 3
#define RACE_DWARF 4
#define RACE_GNOLL 5
#define RACE_HALFING 6
#define RACE_HIGH_ELF 7
#define RACE_HIGH_MEN 8
#define RACE_KLACKON 9
#define RACE_LIZARDMEN 10
#define RACE_NOMAD 11
#define RACE_ORC 12
#define RACE_TROLL 13
#define RACE_COUNT 14

#define CITY_JOB_TRADE 0x0001
#define CITY_JOB_HOUSE 0x0002
#define CITY_JOB_BUILD 0x0003

#define CITY_DATA_SIZE 114
#define BUILD_TYPECOUNT 33
typedef struct _city
{
	char name[12];
	aword unknown0; /* always 0x0000 ? */
	abyte race, locx, locy, world, owner;
	abyte clvl, popk; /* max_popk=25 */
	abyte flag; /* NOT a valid city if zero? NOPE! */
	abyte unknown1[6];
	aword currjob; /* current build job */
	aword unknown2; /* 0x0020 - when building? */
	aword unknown3; /* always 0xffff ? */
	abyte buildings[BUILD_TYPECOUNT];
	abyte unknown[47];
}
city;

const abyte BUILDINGS[RACE_COUNT][BUILD_TYPECOUNT] = {
//BARBARIAN 0
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//BEASTMAN 1
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x01,0x01,0xff,0x01,0xff,
	0xff,0x01,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,0x00,
	0x01,0x00,0x01,0xff,0x00,0x01,0x01,0x01,0x01,0x01,0x01
	},
//DARK_ELF 2
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x01,
	0xff,0x01,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,0x01,
	0xff,0x00,0x00,0x01,0x00,0x01,0x01,0x01,0x01,0x01,0x01
	},
//DRACONIAN 3
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x01,0x01,0xff,0x00,0x01,
	0xff,0x01,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,0x00,
	0x01,0x00,0x00,0x01,0x00,0x01,0x01,0x01,0xff,0x01,0x01
	},
//DWARF 4
	{
	0x00,0x01,0x00,0x01,0xff,0x01,0xff,0xff,0xff,0xff,0xff,
	0xff,0x01,0x01,0x01,0xff,0x01,0xff,0xff,0x00,0x01,0xff,
	0xff,0x01,0xff,0xff,0x00,0x01,0x01,0x01,0xff,0x01,0x01
	},
//GNOLL 5
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//HALFING 6
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//HIGH_ELF 7
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x01,
	0xff,0x01,0x00,0x01,0xff,0x00,0x01,0x01,0x00,0x01,0xff,
	0xff,0x00,0x00,0x01,0x00,0x01,0x01,0x01,0x01,0x01,0x01
	},
//HIGH_MEN 8
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x01,0x01,0xff,0x00,0x00,
	0x01,0x01,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,0x00,
	0x01,0x00,0x00,0x01,0x00,0x01,0x01,0x01,0x01,0x01,0x01
	},
//KLACKON 9
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//LIZARDMEN 10
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//NOMAD 11
	{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	},
//ORC 12
	{
	0x00,0x01,0x00,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,
	0x01,0x01,0x00,0x01,0x01,0x00,0x01,0x01,0x00,0x00,0x00,
	0x01,0x00,0x00,0x01,0x00,0x01,0x01,0x01,0x01,0x01,0x01
	},
//TROLL 13
	{
	0x00,0x01,0x00,0x01,0xff,0x01,0x01,0x01,0xff,0x01,0xff,
	0xff,0x01,0x01,0xff,0xff,0xff,0xff,0xff,0x00,0x00,0x00,
	0x01,0x01,0xff,0xff,0x00,0x01,0x01,0x01,0xff,0xff,0x01
	}
};

#define UNIT_DATA_SIZE 32
typedef struct _unit
{
	abyte locx, locy, world, owner, huh0, type;
	abyte heronum; /* index in wizard 0-6 */
	abyte active, movesleft, dstx, dsty, action; /* 12 bytes */
	abyte level, huh1;
	aword experience;
	abyte huh2, hitstaken; /* 18 bytes */
	abyte unknown1[5];
	abyte mutation, spells[4], isbuilding;
	abyte unknown[3];
}
unit;

/* elite unit */
#define UNIT_LVL_MAX 3
#define UNIT_EXP_MAX 120
/* demi-god hero */
#define HERO_LVL_MAX 8
#define HERO_EXP_MAX 1000

#define UNIT_WEAPON_NORMAL 0x00
#define UNIT_WEAPON_MAGIC 0x01
#define UNIT_WEAPON_MITHRIL 0x02
#define UNIT_WEAPON_ADAMANTIUM 0x03

#define UNIT_BARBARIAN_BEGIN 0x27
#define UNIT_BARBARIAN_END 0x2d
#define UNIT_BEASTMEN_BEGIN 0x2e
#define UNIT_BEASTMEN_END 0x38
#define UNIT_DARK_ELF_BEGIN 0x39
#define UNIT_DARK_ELF_END 0x41
#define UNIT_DRACONIAN_BEGIN 0x42
#define UNIT_DRACONIAN_END 0x4b
#define UNIT_DWARVEN_BEGIN 0x4c
#define UNIT_DWARVEN_END 0x52
#define UNIT_GNOLL_BEGIN 0x53
#define UNIT_GNOLL_END 0x58
#define UNIT_HALFLING_BEGIN 0x59
#define UNIT_HALFLING_END 0x5e
#define UNIT_HIGH_ELF_BEGIN 0x5f
#define UNIT_HIGH_ELF_END 0x67
#define UNIT_HIGH_MEN_BEGIN 0x68
#define UNIT_HIGH_MEN_END 0x71
#define UNIT_KLACKON_BEGIN 0x72
#define UNIT_KLACKON_END 0x77
#define UNIT_LIZARD_BEGIN 0x78
#define UNIT_LIZARD_END 0x7e
#define UNIT_NOMAD_BEGIN 0x7f
#define UNIT_NOMAD_END 0x88
#define UNIT_ORC_BEGIN 0x89
#define UNIT_ORC_END 0x92
#define UNIT_TROLL_BEGIN 0x93
#define UNIT_TROLL_END 0x99

#define UNIT_CHOSEN_ONE 0x22
#define UNIT_COMMON_TRIREME 0x23
#define UNIT_COMMON_GALLEY 0x24
#define UNIT_COMMON_CATAPULT 0x25
#define UNIT_COMMON_WARSHIP 0x26
#define UNIT_BARBARIAN_SPEARMEN 0x27
#define UNIT_BARBARIAN_SETTLERS 0x2c
#define UNIT_BARBARIAN_BESERKERS 0x2d
#define UNIT_BEASTMEN_SPEARMEN 0x2e
#define UNIT_BEASTMEN_CENTAURS 0x36
#define UNIT_BEASTMEN_MANTICORES 0x37
#define UNIT_BEASTMEN_MINOTAURS 0x38
#define UNIT_DARK_ELF_SPEARMEN 0x39
#define UNIT_DARK_ELF_NIGHTMARES 0x41
#define UNIT_DRACONIAN_SPEARMEN 0x42
#define UNIT_DRACONIAN_SWORDSMEN 0x43
#define UNIT_DRACONIAN_SETTLERS 0x49
#define UNIT_DRACONIAN_DOOM_DRAKES 0x4a
#define UNIT_DRACONIAN_AIR_SHIP 0x4b
#define UNIT_DWARVEN_SWORDSMEN 0x4c
#define UNIT_DWARVEN_HAMMERHANDS 0x4f
#define UNIT_DWARVEN_STEAM_CANNON 0x50
#define UNIT_DWARVEN_GOLEM 0x51
#define UNIT_DWARVEN_SETTLERS 0x52
#define UNIT_GNOLL_SPEARMEN 0x53
#define UNIT_GNOLL_WOLF_RIDERS 0x58
#define UNIT_HALFLING_SPEARMEN 0x59
#define UNIT_HALFLING_SWORDSMEN 0x5a
#define UNIT_HALFLING_SLINGERS 0x5e
#define UNIT_HIGH_ELF_SPEARMEN 0x5f
#define UNIT_HIGH_ELF_PEGASAI 0x67
#define UNIT_HIGH_MEN_SPEARMEN 0x68
#define UNIT_HIGH_MEN_SWORDSMEN 0x69
#define UNIT_HIGH_MEN_BOWMEN 0x6a
#define UNIT_HIGH_MEN_CAVALRY 0x6b
#define UNIT_HIGH_MEN_PRIESTS 0x6c
#define UNIT_HIGH_MEN_MAGICIANS 0x6d
#define UNIT_HIGH_MEN_ENGINEERS 0x6e
#define UNIT_HIGH_MEN_SETTLERS 0x6f
#define UNIT_HIGH_MEN_PIKEMEN 0x70
#define UNIT_HIGH_MEN_PALADINS 0x71
#define UNIT_KLACKON_SPEARMEN 0x72
#define UNIT_KLACKON_STAG_BEETLE 0x77
#define UNIT_LIZARD_SPEARMEN 0x78
#define UNIT_LIZARD_DRAGON_TURTLE 0x7e
#define UNIT_NOMAD_SPEARMEN 0x7f
#define UNIT_NOMAD_GRIFFINS 0x88
#define UNIT_ORC_SPEARMEN 0x89
#define UNIT_ORC_SWORDSMEN 0x8A
#define UNIT_ORC_ENGINEERS 0x90
#define UNIT_ORC_SETTLERS 0x91
#define UNIT_ORC_WYVERN_RIDERS 0x92
#define UNIT_TROLL_SPEARMEN 0x93
#define UNIT_TROLL_WAR_MAMMOTH 0x99
#define UNIT_SUMMON_MAGIC_SPIRIT 0x9a

/* magic save data size = 123300 (0x1E1A4) bytes */
#define MAGIC_DATA_SIZE 123300
typedef struct _magic
{
	abyte mfile[MAGIC_DATA_SIZE];
}
magic;

int done_city(city* pcity)
{
	if(!pcity->locx||!pcity->locy) return -1;
	if(!pcity->name[0]) return -1;
	return 0;
}

int skip_city(city* pcity, int index, char* pname)
{
	if(!pcity->popk&&!pcity->flag) return -1;
	if(pcity->owner!=index) return -1;
	if(pname&&strcasecmp(pname,pcity->name)) return -1;
	return 0;
}

void show_wizard(magic* pdata, int index)
{
	int loop, init, test;
	wizz *pwizz = (wizz*)
		&pdata->mfile[OFFSET_WIZZ_INIT+index*WIZZ_DATA_SIZE];
	if(index<5)
	{
		printf("WIZARD => %s (Fame: %d), ",pwizz->name,pwizz->fame);
		printf("Gold: %d, Mana: %d\n",pwizz->gold,pwizz->mana);
		printf("Casting(P): %d, Casting(A): %d\n",
			pwizz->casting_skill_personal,pwizz->casting_skill_adjusted);
		printf("Ability: ");
		for(loop=0,init=0;loop<18;loop++)
		{
			switch(loop)
			{
			case ABILITY_ALCHEMY:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Alchemy");
					init++;
				}
				break;
			case ABILITY_WARLORD:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Warlord");
					init++;
				}
				break;
			case ABILITY_CHAOS_MASTERY:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Chaos Mastery");
					init++;
				}
				break;
			case ABILITY_NATURE_MASTERY:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Nature Mastery");
					init++;
				}
				break;
			case ABILITY_SORCERY_MASTERY:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Sorcery Mastery");
					init++;
				}
				break;
			case ABILITY_INFERNAL_POWER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Infernal Power");
					init++;
				}
				break;
			case ABILITY_DIVINE_POWER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Divine Power");
					init++;
				}
				break;
			case ABILITY_SAGE_MASTER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Sage Master");
					init++;
				}
				break;
			case ABILITY_CHANNELLER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Channeller");
					init++;
				}
				break;
			case ABILITY_MYRRAN_:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Myrran");
					init++;
				}
				break;
			case ABILITY_ARCHMAGE:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Archmage");
					init++;
				}
				break;
			case ABILITY_MANA_FOCUSING:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Mana Focusing");
					init++;
				}
				break;
			case ABILITY_NODE_MASTERY:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Node Mastery");
					init++;
				}
				break;
			case ABILITY_FAMOUS:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Famous");
					init++;
				}
				break;
			case ABILITY_RUNEMASTER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Runemaster");
					init++;
				}
				break;
			case ABILITY_CONJURER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Conjurer");
					init++;
				}
				break;
			case ABILITY_CHARISMATIC:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Charismatic");
					init++;
				}
				break;
			case ABILITY_ARTIFICIER:
				if(pwizz->abilities[loop])
				{
					if(init) printf(", ");
					printf("Artificier");
					init++;
				}
				break;
			}
		}
		printf("\n");
		printf("SpellBook(s): ");
		init=0;
		if(pwizz->book_nature>0)
		{
			if(init) printf(", ");
			printf("Nature (%d)",pwizz->book_nature);
			init++;
		}
		if(pwizz->book_sorcery>0)
		{
			if(init) printf(", ");
			printf("Sorcery (%d)",pwizz->book_sorcery);
			init++;
		}
		if(pwizz->book_chaos>0)
		{
			if(init) printf(", ");
			printf("Chaos (%d)",pwizz->book_chaos);
			init++;
		}
		if(pwizz->book_life>0)
		{
			if(init) printf(", ");
			printf("Life (%d)",pwizz->book_life);
			init++;
		}
		if(pwizz->book_death>0)
		{
			if(init) printf(", ");
			printf("Death (%d)",pwizz->book_death);
			init++;
		}
		printf("\n");
	}
	else
	{
		printf("WIZARD => INDEPENDENT\n");
	}
	for(loop=0,init=0,test=0;loop<CITY_COUNT_MAX;loop++)
	{
		city *pcity = (city*)
			&pdata->mfile[OFFSET_CITY_INIT+loop*CITY_DATA_SIZE];
		if(done_city(pcity)) break;
		if(skip_city(pcity,index,0x0))
		{
			if(!pcity->popk&&!pcity->flag) test++;
			continue;
		}
		init++;
	}
	printf("Capitals/Cities/Towns/Hamlets: %d (%d), ",init,loop-test);
	for(loop=0,init=0,test=0;loop<UNIT_COUNT_MAX;loop++)
	{
		unit *punit = (unit*)
			&pdata->mfile[OFFSET_UNIT_INIT+loop*UNIT_DATA_SIZE];
		if(!punit->locx||!punit->locy) break;
		if(punit->owner==index) init++;
		if(punit->owner>5&&punit->active) test++;
	}
	printf("Units: %d (%d) {%d}\n\n",init,loop,test);
}

void upgrade_wizard(magic* pdata, int index)
{
	int loop;
	wizz *pwizz = (wizz*)
		&pdata->mfile[OFFSET_WIZZ_INIT+index*WIZZ_DATA_SIZE];
	aword *pturn = (aword*) &pdata->mfile[OFFSET_TURN];
	/* upgrade wizard */
	pwizz->mana = MAX_MANA;
	pwizz->gold = MAX_GOLD;
	pwizz->casting_skill_adjusted = SET_CAST; /* lasts current turn only */
	if(*pturn==0) /* only at the beginning... */
	{
		/*
		pwizz->fame = SET_FAME;
		*/
		pwizz->abilities[ABILITY_ARCHMAGE] = 1;
		pwizz->abilities[ABILITY_WARLORD] = 1;
		pwizz->abilities[ABILITY_NODE_MASTERY] = 1;
		pwizz->abilities[ABILITY_ALCHEMY] = 1;
		pwizz->abilities[ABILITY_MANA_FOCUSING] = 1;
		/* knows ALL spells */
		for(loop=0;loop<40;loop++)
			pwizz->spells_nature[loop] = SPELL_KNOWN;
		for(loop=0;loop<40;loop++)
			pwizz->spells_sorcery[loop] = SPELL_KNOWN;
		for(loop=0;loop<40;loop++)
			pwizz->spells_chaos[loop] = SPELL_KNOWN;
		for(loop=0;loop<40;loop++)
			pwizz->spells_life[loop] = SPELL_KNOWN;
		for(loop=0;loop<40;loop++)
			pwizz->spells_death[loop] = SPELL_KNOWN;
		for(loop=0;loop<14;loop++)
			pwizz->spells_arcane[loop] = SPELL_KNOWN;
	}
	printf("Wizard '%s' upgraded!\n\n",pwizz->name);
}

void show_city(magic* pdata, int index, char* pname)
{
	int next;
	for(next=0;next<CITY_COUNT_MAX;next++)
	{
		int loop, init, bmax = 0;
		city *pcity = (city*)
			&pdata->mfile[OFFSET_CITY_INIT+next*CITY_DATA_SIZE];
		if(done_city(pcity)) break;
		if(skip_city(pcity,index,pname)) continue;
		printf("CITY => %s (%d,%d)[%s]{%d}, Population: %d (%d)\n",pcity->name,
			pcity->locx,pcity->locy,pcity->world?"Myrran":"Arcanus",next,
			pcity->popk,pcity->clvl);
		printf("Race: ");
		switch(pcity->race)
		{
		case RACE_BARBARIAN: printf("Barbarian"); break;
		case RACE_BEASTMAN: printf("Beastman"); break;
		case RACE_DARK_ELF: printf("Dark Elf"); break;
		case RACE_DRACONIAN: printf("Draconian"); break;
		case RACE_DWARF: printf("Dwarf"); break;
		case RACE_GNOLL: printf("Gnoll"); break;
		case RACE_HALFING: printf("Halfling"); break;
		case RACE_HIGH_ELF: printf("High Elf"); break;
		case RACE_HIGH_MEN: printf("High Men"); break;
		case RACE_KLACKON: printf("Klackon"); break;
		case RACE_LIZARDMEN: printf("Lizardmen"); break;
		case RACE_NOMAD: printf("Nomad"); break;
		case RACE_ORC: printf("Orc"); break;
		case RACE_TROLL: printf("Troll"); break;
		default: printf("Unknown"); break;
		}
		printf(", Owner: ");
		if(pcity->owner<5)
		{
			wizz *pwizz = (wizz*)
				&pdata->mfile[OFFSET_WIZZ_INIT+pcity->owner*WIZZ_DATA_SIZE];
			printf("%s",pwizz->name);
		}
		else printf("Unknown");
		printf(", Current Job: ");
		switch(pcity->currjob)
		{
		case CITY_JOB_TRADE: printf("Trade Goods"); break;
		case CITY_JOB_HOUSE: printf("Housing"); break;
		case CITY_JOB_BUILD+BUILD_BARRACKS: printf("Barracks"); break;
		case CITY_JOB_BUILD+BUILD_ARMORY: printf("Armory"); break;
		case CITY_JOB_BUILD+BUILD_FIGHTERS_GUILD:
			printf("Fighters Guild"); break;
		case CITY_JOB_BUILD+BUILD_ARMORERS_GUILD:
			printf("Armorers Guild"); break;
		case CITY_JOB_BUILD+BUILD_WAR_COLLEGE: printf("War College"); break;
		case CITY_JOB_BUILD+BUILD_SMITHY: printf("Smithy"); break;
		case CITY_JOB_BUILD+BUILD_STABLES: printf("Stables"); break;
		case CITY_JOB_BUILD+BUILD_ANIMIST_GUILD:
			printf("Animist Guild"); break;
		case CITY_JOB_BUILD+BUILD_FANTASTIC_STABLE:
			printf("Fantastic Stable"); break;
		case CITY_JOB_BUILD+BUILD_SHIP_WRIGHTS_GUILD:
			printf("Ship Wrights Guild"); break;
		case CITY_JOB_BUILD+BUILD_SHIP_YARD: printf("Ship Yard"); break;
		case CITY_JOB_BUILD+BUILD_MARITIME_GUILD:
			printf("Maritime Guild"); break;
		case CITY_JOB_BUILD+BUILD_SAWMILL: printf("Sawmill"); break;
		case CITY_JOB_BUILD+BUILD_LIBRARY: printf("Library"); break;
		case CITY_JOB_BUILD+BUILD_SAGES_GUILD: printf("Sages Guild"); break;
		case CITY_JOB_BUILD+BUILD_ORACLE: printf("Oracle"); break;
		case CITY_JOB_BUILD+BUILD_ALCHEMISTS_GUILD:
			printf("Alchemist Guild"); break;
		case CITY_JOB_BUILD+BUILD_UNIVERSITY: printf("University"); break;
		case CITY_JOB_BUILD+BUILD_WIZARDS_GUILD:
			printf("Wizards Guild"); break;
		case CITY_JOB_BUILD+BUILD_SHRINE: printf("Shrine"); break;
		case CITY_JOB_BUILD+BUILD_TEMPLE: printf("Temple"); break;
		case CITY_JOB_BUILD+BUILD_PARTHENON: printf("Parthenon"); break;
		case CITY_JOB_BUILD+BUILD_CATHEDRAL: printf("Cathedral"); break;
		case CITY_JOB_BUILD+BUILD_MARKETPLACE: printf("Marketplace"); break;
		case CITY_JOB_BUILD+BUILD_BANK: printf("Bank"); break;
		case CITY_JOB_BUILD+BUILD_MERCHANTS_GUILD:
			printf("Merchants Guild"); break;
		case CITY_JOB_BUILD+BUILD_GRANARY: printf("Granary"); break;
		case CITY_JOB_BUILD+BUILD_FARMERS_MARKET:
			printf("Farmers Market"); break;
		case CITY_JOB_BUILD+BUILD_FORESTERS_GUILD:
			printf("Foresters Guild"); break;
		case CITY_JOB_BUILD+BUILD_BUILDERS_HALL:
			printf("Builders Hall"); break;
		case CITY_JOB_BUILD+BUILD_MECHANICIAN_GUILD:
			printf("Mechanician Guild"); break;
		case CITY_JOB_BUILD+BUILD_MINERS_GUILD: printf("Miners Guild"); break;
		case CITY_JOB_BUILD+BUILD_CITY_WALLS: printf("City Walls"); break;
		default: printf("[%04x]",pcity->currjob);
		}
		printf(" (%04x), ",pcity->unknown2);
		printf("Flag: [%02x], ",pcity->flag);
		for(loop=0;loop<6;loop++)
			printf("[%02x]",pcity->unknown1[loop]);
		/* check available building types */
		if(pcity->race<RACE_COUNT)
		{
			for(loop=0;loop<BUILD_TYPECOUNT;loop++)
			{
				if(BUILDINGS[pcity->race][loop]!=0xff)
					bmax++;
			}
		}
		printf("\nBuildings: ");
		for(loop=0,init=0;loop<BUILD_TYPECOUNT;loop++)
		{
			switch(loop)
			{
			case BUILD_BARRACKS:
				if(pcity->buildings[loop]!=0xff)
				{
					if(init) printf(", ");
					printf("Barracks");
					init++;
				}
				break;
			case BUILD_ARMORY:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Armory");
					init++;
				}
				break;
			case BUILD_FIGHTERS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Fighters Guild");
					init++;
				}
				break;
			case BUILD_ARMORERS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Armorers Guild");
					init++;
				}
				break;
			case BUILD_WAR_COLLEGE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("War College");
					init++;
				}
				break;
			case BUILD_SMITHY:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Smithy");
					init++;
				}
				break;
			case BUILD_STABLES:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Stables");
					init++;
				}
				break;
			case BUILD_ANIMIST_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Animist Guild");
					init++;
				}
				break;
			case BUILD_FANTASTIC_STABLE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Fantastic Stable");
					init++;
				}
				break;
			case BUILD_SHIP_WRIGHTS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Ship Wrights Guild");
					init++;
				}
				break;
			case BUILD_SHIP_YARD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Ship Yard");
					init++;
				}
				break;
			case BUILD_MARITIME_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Maritime Guild");
					init++;
				}
				break;
			case BUILD_SAWMILL:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Sawmill");
					init++;
				}
				break;
			case BUILD_LIBRARY:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Library");
					init++;
				}
				break;
			case BUILD_SAGES_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Sages Guild");
					init++;
				}
				break;
			case BUILD_ORACLE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Oracle");
					init++;
				}
				break;
			case BUILD_ALCHEMISTS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Alchemist Guild");
					init++;
				}
				break;
			case BUILD_UNIVERSITY:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("University");
					init++;
				}
				break;
			case BUILD_WIZARDS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Wizards Guild");
					init++;
				}
				break;
			case BUILD_SHRINE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Shrine");
					init++;
				}
				break;
			case BUILD_TEMPLE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Temple");
					init++;
				}
				break;
			case BUILD_PARTHENON:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Parthenon");
					init++;
				}
				break;
			case BUILD_CATHEDRAL:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Cathedral");
					init++;
				}
				break;
			case BUILD_MARKETPLACE:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Marketplace");
					init++;
				}
				break;
			case BUILD_BANK:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Bank");
					init++;
				}
				break;
			case BUILD_MERCHANTS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Merchants Guild");
					init++;
				}
				break;
			case BUILD_GRANARY:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Granary");
					init++;
				}
				break;
			case BUILD_FARMERS_MARKET:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Farmers Market");
					init++;
				}
				break;
			case BUILD_FORESTERS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Foresters Guild");
					init++;
				}
				break;
			case BUILD_BUILDERS_HALL:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Builders Hall");
					init++;
				}
				break;
			case BUILD_MECHANICIAN_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Mechanician Guild");
					init++;
				}
				break;
			case BUILD_MINERS_GUILD:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("Miners Guild");
					init++;
				}
				break;
			case BUILD_CITY_WALLS:
				if(pcity->buildings[loop]!=0xFF)
				{
					if(init) printf(", ");
					printf("City Walls");
					init++;
				}
				break;
			}
		}
		printf(" (%d/%d)\n",init,bmax);
		printf("Building Codes: ");
		for(loop=0,init=0;loop<BUILD_TYPECOUNT-1;loop++)
			printf("0x%02x,",pcity->buildings[loop]);
		printf("0x%02x\n\n",pcity->buildings[loop]);
		if(!pname)
		{
			char check[40];
			printf("[1] Continue [2] Exit --> Choice[1]: ");
			fgets(check,40,stdin);
			putchar('\n');
			if(check[0]=='2'&&(check[1]=='\n'||check[1]=='\0'))
				break;
		}
		else if(!strcasecmp(pname,pcity->name)) break;
	}
}

void upgrade_city(magic* pdata, int index, char* pname)
{
	int next, modd, loop, step;
	for(next=0,modd=0;next<CITY_COUNT_MAX;next++)
	{
		city *pcity = (city*)
			&pdata->mfile[OFFSET_CITY_INIT+next*CITY_DATA_SIZE];
		if(done_city(pcity)) break;
		if(skip_city(pcity,index,pname)) continue;
		/* count buildings */
		for(loop=0,step=0;loop<BUILD_TYPECOUNT;loop++)
			if(pcity->buildings[loop]!=0xff) step++;
		/* upgrade city */
		if(pcity->popk<SET_POP1||pcity->clvl<SET_LVL1||step<10)
		{
			if(pcity->popk<SET_POP1)
			{
				pcity->popk = SET_POP1;
				pcity->clvl = SET_LVL1;
			}
			if(step<10)
			{
				switch(pcity->race)
				{
				case RACE_BEASTMAN:
				case RACE_DRACONIAN:
				case RACE_HIGH_ELF:
				case RACE_HIGH_MEN:
				case RACE_ORC:
				case RACE_TROLL:
					for(loop=0;loop<BUILD_TYPECOUNT;loop++)
						pcity->buildings[loop] = BUILDINGS[pcity->race][loop];
					break;
				}
			}
			printf("City '%s' upgraded!\n\n",pcity->name);
			modd=1;
		}
	}
	if(!modd) printf("No city needs upgrade!\n\n");
}

void show_unit(magic* pdata,int index)
{
	int loop, init;
	for(loop=0,init=0;loop<UNIT_COUNT_MAX;loop++)
	{
		unit *punit = (unit*)
			&pdata->mfile[OFFSET_UNIT_INIT+loop*UNIT_DATA_SIZE];
		if(!punit->locx||!punit->locy) break;
		if(punit->owner!=index) continue;
		printf("UNIT => ");
		printf("Location: (%d,%d)[%d]\n",punit->locx,punit->locy,punit->world);
		printf("Owner: ");
		if(punit->owner<5)
		{
			wizz *pwizz = (wizz*)
				&pdata->mfile[OFFSET_WIZZ_INIT+punit->owner*WIZZ_DATA_SIZE];
			printf("%s",pwizz->name);
		}
		else printf("Unknown (%d)",punit->owner);
		printf(", Type: 0x%02x\n",punit->type);
		printf("Level: %d, Expr: %d\n",punit->level,punit->experience);
		printf("Hero#: %d, Active: %d\n\n",punit->heronum,punit->active);
		init++;
		if(init%4==0)
		{
			char check[40];
			printf("[1] Continue [2] Exit --> Choice[1]: ");
			fgets(check,40,stdin);
			putchar('\n');
			if(check[0]=='2'&&(check[1]=='\n'||check[1]=='\0'))
				break;
		}
	}
}

void upgrade_unit(magic* pdata,int index)
{
	int loop, modd;
	for(loop=0,modd=0;loop<UNIT_COUNT_MAX;loop++)
	{
		unit *punit = (unit*)
			&pdata->mfile[OFFSET_UNIT_INIT+loop*UNIT_DATA_SIZE];
		if(!punit->locx||!punit->locy) break;
		if(punit->owner!=index) continue;
		if(punit->heronum==0xff)
		{
			int test = 0;
			if(punit->level<UNIT_LVL_MAX||punit->experience<UNIT_EXP_MAX)
			{
				punit->level = UNIT_LVL_MAX;
				punit->experience = UNIT_EXP_MAX;
				test++;
			}
			if(!(punit->mutation&UNIT_WEAPON_ADAMANTIUM))
			{
				punit->mutation |= UNIT_WEAPON_ADAMANTIUM;
				test++;
			}
			if(test)
			{
				printf("Unit '0x%02x' upgraded!\n\n",punit->type);
				modd++;
			}
		}
		else
		{
			wizz *pwizz = (wizz*)
				&pdata->mfile[OFFSET_WIZZ_INIT+punit->owner*WIZZ_DATA_SIZE];
			hero *phero = (hero*) &pwizz->heroes[punit->heronum][0];
			if(punit->level<HERO_LVL_MAX||punit->experience<HERO_EXP_MAX)
			{
				/* how to set noble hero??? */
				punit->level = HERO_LVL_MAX;
				punit->experience = HERO_EXP_MAX;
				punit->mutation |= UNIT_WEAPON_ADAMANTIUM;
				printf("Hero [%s] upgraded!\n\n",phero->name);
				modd++;
			}
		}
	}
	if(!modd) printf("No unit needs upgrade!\n\n");
}

#define ERROR_INVALID_PARAM -1
#define ERROR_PARAM_SLOT -2
#define ERROR_PARAM_CITY -3
#define ERROR_STRUCT_WIZZ -4
#define ERROR_STRUCT_CITY -5
#define ERROR_STRUCT_UNIT -6
#define ERROR_INVALID_TASK -7
#define ERROR_PARAM_WIZZ -8

#define TASK_LIST_DATA 0
#define TASK_LIST_CITY 1
#define TASK_LIST_UNIT 2
#define TASK_CHEAT 10
#define TASK_CHEAT_DATA 10
#define TASK_CHEAT_CITY 11
#define TASK_CHEAT_UNIT 12
#define TASK_CHEAT_ALL 13

int main(int argc, char *argv[])
{
	FILE* pFile;
	int slot = 1, task = 0, wsel = 0, loop, turn;
	int cheat = 0, cfull = 0;
	char savegame[16];
	char *pname_city = 0x0;
	aword *pword;
	magic mdata;

	for(loop=1;loop<argc;loop++)
	{
		if(!strcmp(argv[loop],"--slot"))
		{
			loop++;
			if(loop<argc)
				slot = atoi(argv[loop]);
			else
			{
				printf("Cannot get slot parameter!\n");
				return ERROR_PARAM_SLOT;
			}
		}
		else if(!strcmp(argv[loop],"--wizz"))
		{
			loop++;
			if(loop<argc)
				wsel = atoi(argv[loop]);
			if(wsel<0||wsel>4)
			{
				printf("Invalid wizard index!\n");
				return ERROR_PARAM_WIZZ;
			}
		}
		else if(!strcmp(argv[loop],"--city"))
		{
			loop++;
			if(loop<argc)
			{
				pname_city = argv[loop];
				task = TASK_LIST_CITY;
			}
			else
			{
				printf("Cannot get city parameter!\n");
				return ERROR_PARAM_CITY;
			}
		}
		else if(!strcmp(argv[loop],"--cheat"))
		{
			cheat = 1;
		}
		else if(!strcmp(argv[loop],"--cheatall"))
		{
			cheat = 1; cfull = 1;
		}
		else
		{
			if(!task)
			{
				if(!strcmp(argv[loop],"city"))
				{
					task = TASK_LIST_CITY;
				}
				else if(!strcmp(argv[loop],"unit"))
				{
					task = TASK_LIST_UNIT;
				}
				else
				{
					printf("Invalid task '%s'! Aborting!\n",argv[loop]);
					return ERROR_INVALID_TASK;
				}
			}
			else
			{
				printf("Invalid parameter '%s'! Aborting!\n",argv[loop]);
				return ERROR_INVALID_PARAM;
			}
		}
	}

	if(sizeof(wizz)!= WIZZ_DATA_SIZE)
	{
		printf("Wizard data structure size error! (%d/%d)\n",
			(int)sizeof(wizz),WIZZ_DATA_SIZE);
		return ERROR_STRUCT_WIZZ;
	}
	if(sizeof(city)!= CITY_DATA_SIZE)
	{
		printf("City data structure size error! (%d/%d)\n",
			(int)sizeof(city),CITY_DATA_SIZE);
		return ERROR_STRUCT_CITY;
	}
	if(sizeof(unit)!= UNIT_DATA_SIZE)
	{
		printf("Unit data structure size error! (%d/%d)\n",
			(int)sizeof(unit),UNIT_DATA_SIZE);
		return ERROR_STRUCT_UNIT;
	}

	/* copy the file name */
	sprintf(savegame,"SAVE%d.GAM",slot);
	/* open file */
	pFile = fopen(savegame,"rb+");
	if(pFile)
	{
		/* read the whole file? */
		fseek(pFile,0,SEEK_SET);
		fread((void*)&mdata,sizeof(magic),1,pFile);
		/* game info */
		pword = (aword*) &mdata.mfile[OFFSET_TURN];
		turn = *pword;
		printf("\nGAME => ");
		printf("Year: %d, Month: %d (%d)\n\n",
			(int)(1400+(turn/12)),(int)(1+(turn%12)),turn);
		/* finalize task */
		if(cheat)
		{
			task += TASK_CHEAT;
			printf("CHEAT MODE!\n\n");
			if(cfull) task = TASK_CHEAT_ALL;
		}
		switch(task)
		{
			default:
			case TASK_LIST_DATA:
			{
				for(loop=0;loop<6;loop++)
					show_wizard(&mdata,loop);
				break;
			}
			case TASK_LIST_CITY:
			{
				show_city(&mdata,wsel,pname_city);
				break;
			}
			case TASK_LIST_UNIT:
			{
				show_unit(&mdata,wsel);
				break;
			}
			case TASK_CHEAT_DATA:
			{
				upgrade_wizard(&mdata,0);
				break;
			}
			case TASK_CHEAT_CITY:
			{
				upgrade_city(&mdata,wsel,pname_city);
				break;
			}
			case TASK_CHEAT_UNIT:
			{
				upgrade_unit(&mdata,0);
				break;
			}
			case TASK_CHEAT_ALL:
			{
				upgrade_wizard(&mdata,0);
				upgrade_city(&mdata,0,0x0);
				upgrade_unit(&mdata,0);
				break;
			}
		}
		if(cheat)
		{
			/* write the whole file? */
			fseek(pFile,0,SEEK_SET);
			fwrite((void*)&mdata,sizeof(magic),1,pFile);
		}
		fclose(pFile);
	}
	else
		printf("File '%s' not found!\n",savegame);

	return 0;
}
