/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
typedef unsigned int word32_t;
typedef unsigned short word16_t;
typedef unsigned char byte08_t;
/*----------------------------------------------------------------------------*/
typedef struct __attribute__ ((packed)) _xcr_head_t
{
	char head[13]; /* xcr File 1.00 */
	byte08_t unk1[7]; /* null */
	word32_t size; /* number of files */
	word32_t alen; /* archive length @ file size */
}
xcr_head_t;
/*----------------------------------------------------------------------------*/
typedef struct __attribute__ ((packed)) _xcr_path_t
{
	char name[256]; /* ascii? */
	char path[256]; /* wchar? */
	word32_t offs; /* offset */
	word32_t size; /* size */
	byte08_t unk1[12]; /* ??? */
}
xcr_path_t;
/*----------------------------------------------------------------------------*/
typedef struct __attribute__ ((packed)) _xcr_hero_t
{
	/* 4328 bytes - looks encoded... no name string can be found! */
}
xcr_hero_t;
/*----------------------------------------------------------------------------*/
typedef struct __attribute__ ((packed)) _xcr_file_t
{
	xcr_head_t head;
	xcr_path_t path;
	/** multiple xcr_path_t + data */
}
xcr_file_t;
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
	int loop;
	char *psrc;
	FILE* file;
	xcr_file_t data;
	long int temp, save;
	/* size check */
	printf("@@ xcrHead Size: %d\n",(int)sizeof(xcr_head_t));
	printf("@@ xcrPath Size: %d\n",(int)sizeof(xcr_path_t));
	if (argc<2) return 0;
	psrc = argv[1];
	file = fopen(psrc,"rb");
	if (file)
	{
		do
		{
			if (fread((void*)&data.head,sizeof(xcr_head_t),1,file)!=1)
			{
				fprintf(stderr,"** Cannot read XCR header!\n");
				break;
			}
			printf("-- XCR Header:{%s}\n",data.head.head);
			printf("-- Number of files:[%u]\n",data.head.size);
			/*printf("-- Archive length:[%u]\n",data.head.alen);*/
			save = ftell(file);
			fseek(file,0,SEEK_END);
			temp = ftell(file);
			if (temp!=data.head.alen)
			{
				fprintf(stderr,"** Invalid file size:[%ld/%d]\n",
					temp,data.head.alen);
				break;
			}
			/* go back to saved point */
			fseek(file,save,SEEK_SET);
			for (loop=0;loop<data.head.size;loop++)
			{
				if (fread((void*)&data.path,sizeof(xcr_path_t),1,file)!=1)
				{
					fprintf(stderr,"** Cannot read XCR path!\n");
					break;
				}
				printf("## File:{%s}\n",data.path.name);
				//printf("-- Path:{%s}\n",data.path.path);
				printf("-- Offs:{%u}\n",data.path.offs);
				printf("-- Size:{%u}\n",data.path.size);
			}
		}
		while (0);
		fclose(file);
	}
	else fprintf(stderr,"** Cannot open file '%s'!\n",psrc);
	return 0;
}
