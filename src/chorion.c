#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned int adwrd;
typedef unsigned short aword;
typedef unsigned char abyte;

#define TASK_LIST_GAMEDATA 0
#define TASK_LIST_EMPIRE 1
#define TASK_LIST_PLANET 2
#define TASK_CHEAT 10
#define TASK_CHEAT_EMPIRE (TASK_CHEAT+TASK_LIST_EMPIRE)
#define TASK_CHEAT_PLANET (TASK_CHEAT+TASK_LIST_PLANET)

#define GAME_YEAR_INIT 2299

#define TECH_COMPUTER 0
#define TECH_CONSTRUCTION 1
#define TECH_FORCEFIELD 2
#define TECH_PLANETARY 3
#define TECH_PROPULSION 4
#define TECH_WEAPON 5
#define TECH_COUNT 6

/* 0x6c (108) */
#define PLANET_MAX_COUNT 108
/* 0x104 (260) */
#define FLEET_ENROUTE_MAX_COUNT 260
/* 0x64 (100) */
#define TRANSPORT_ENROUTE_MAX_COUNT 100
#define PLAYER_MAX_COUNT 6

/* 0xb8 (184) */
#define PLANET_DATA_SIZE 184
#define PLANET_NAME_SIZE 12
#define PLANET_TYPE_NONE 0
#define PLANET_TYPE_TOXIC 2
#define PLANET_TYPE_INFERNO 3
#define PLANET_TYPE_DEAD 4
#define PLANET_TYPE_MINIMAL 7
#define PLANET_TYPE_DESERT 8
#define PLANET_TYPE_STEPPE 9
#define PLANET_TYPE_ARID 10
#define PLANET_TYPE_OCEAN 11
#define PLANET_TYPE_TERRAN 12
#define PLANET_GROWTH_HOSTILE 0
#define PLANET_GROWTH_NOTHING 1
#define PLANET_GROWTH_FERTILE 2
#define PLANET_SPECIAL_NORMAL 0
#define PLANET_SPECIAL_U_POOR 1
#define PLANET_SPECIAL_POOR 2
#define PLANET_SPECIAL_ARTIFACTS 3
#define PLANET_SPECIAL_RICH 4
#define PLANET_SPECIAL_U_RICH 5
#define PLANET_SPECIAL_4XTECH 6

typedef struct _planet_t
{
	abyte name[PLANET_NAME_SIZE];
	aword posx, posy; /* 0x0c */
	aword star; /* 0x10 => 0-5 */
	aword look; /* 0x12 => 0,6? */
	aword frame; /* 0x14 => 0-49? */
	aword what1; /* 0x16 => 0-4? */
	aword rocks; /* 0x18 => 0=0, 1=1..4, 2=5..7 */
	aword max_pop1; /* 0x1A */
	aword max_pop2; /* 0x1C */
	aword max_pop3; /* 0x1E */
	aword type; /* 0x20 => 0:none,2:toxic,3:inferno,4:dead,7:minimal,8:desert,
		9:steppe,a:arid,b:ocean,d:terran */
	aword battle_bg; /* 0x22 */
	aword info_gfx; /* 0x24 */
	aword growth; /* 0x26 => 0:hostile,1:nothing,2:fertile */
	aword special; /* 0x28 =>0:normal,1:u-poor,2:poor,3:artifacts,4:rich,
		5:u-rich,6:4xtech */
	aword bc_to_eco; /* 0x2A */
	aword bc_to_next_ship; /* 0x2c */
	aword bc_to_next_factory_tenths; /* 0x2e */
	aword reserve1[2]; /* 0x30 */
	aword waste; /* 0x34 */
	aword owner; /* 0x36 */
	aword owner_prev; /* 0x38 */
	aword population; /* 0x3a */
	aword population_prev; /* 0x3c */
	aword factories; /* 0x3e */
	aword actual_production; /* 0x40 - after maintenance? */
	aword total_production; /* 0x42 */
	aword inbound_troopers[6]; /* 0x44 => 0-300 */
	aword slide_SHIP; /* 0x50 */
	aword slide_DEF; /* 0x52 */
	aword slide_IND; /* 0x54 */
	aword slide_ECO; /* 0x56 */
	aword slide_TECH; /* 0x58 */
	aword build_ship; /* 0x5a - ??? 0-5, 6:star_gate */
	aword planetary_index; /* 0x5c */
	aword missile_bases; /* 0x5e */
	aword bc_to_next_shield; /* 0x60 */
	aword bc_to_upgrade_base; /* 0x62 */
	aword have_stargate; /* 0x64 => 0-1 */
	aword unused1; /* 0x66 => 0! */
	aword shields; /* 0x68 => (5,10,15,20) */
	aword bc_to_next_shields; /* 0x6A */
	aword transport_count; /* 0x6C */
	aword transport_destination; /* 0x6E */
	aword population_tenths; /* 0x70 - byte? */
	aword slide_lock_SHIP; /* 0x72 */
	aword slide_lock_DEF; /* 0x74 */
	aword slide_lock_IND; /* 0x76 */
	aword slide_lock_ECO; /* 0x78 */
	aword slide_lock_TECH; /* 0x7A */
	aword explored[PLAYER_MAX_COUNT]; /* 0x7C => 0-1 */
	aword within_scanner[PLAYER_MAX_COUNT]; /* 0x88 => 0-1 */
	aword within_fuelrange[PLAYER_MAX_COUNT]; /* 0x94 => 0-1, 2:with reserve */
	aword claimed; /* 0xA0 => 0-5 */
	aword total_inbound_troopers[PLAYER_MAX_COUNT]; /* 0xA2 */
	aword colonist_factories; /* 0xAE */
	aword bc_to_industry_refit; /* 0xB0 */
	aword rebels; /* 0xB2 */
	aword unrest; /* 0xB4 => 0:none,1:unrest,3:rebellion */
	aword unrest_reported; /* 0xB6 => 0-1 */
}
planet_t;

#define SHIP_TYPE_COUNT 6

/* 0x1c (28) */
#define FLEET_ENROUTE_DATA_SIZE 28

typedef struct _fleet_enroute_t
{
	aword owner; /* 0-5? -1 unused? */
	aword posx, posy;
	aword destination; /* planetary index */
	aword speed; /* 1-byte? */
	aword ship_count[SHIP_TYPE_COUNT];
	abyte visible[PLAYER_MAX_COUNT]; /* visible to player n */
}
fleet_enroute_t;

/* 0x12 (18) */
#define TRANSPORT_ENROUTE_DATA_SIZE 18

typedef struct _transport_enroute_t
{
	aword owner; /* 0-5? -1 unused? */
	aword posx, posy;
	aword destination; /* planetary index */
	aword population;
	abyte visible[PLAYER_MAX_COUNT]; /* visible to player n */
	aword speed; /* 1-byte? */
}
transport_enroute_t;

/** empire/tech/orbitfleet => player data */

/* 0x332 (818) */
#define EMPIRE1_DATA_SIZE 818

typedef struct __attribute__ ((packed)) _empire1_data_t
{
	aword race; /* 0x00 */
	aword banner; /* 0x02 */
	aword trait1; /* 0x04 => 0-5 */
	aword trait2; /* 0x06 => 0-5 */
	aword ai_turn_p3; /* 0x08 => 0-6 */
	aword ai_turn_p2; /* 0x0A => 0-20 */
	aword empire_within_fuelrange[PLAYER_MAX_COUNT]; /* 0x0C => 0-1 */
	aword relation1[PLAYER_MAX_COUNT]; /* 0x18 */
	aword relation2[PLAYER_MAX_COUNT]; /* 0x24 */
	aword dip_type[PLAYER_MAX_COUNT]; /* 0x30 */
	aword dip_value[PLAYER_MAX_COUNT]; /* 0x3C */
	aword dip_param1[PLAYER_MAX_COUNT]; /* 0x48 */
	aword dip_param2[PLAYER_MAX_COUNT]; /* 0x54 */
	aword unknown1[PLAYER_MAX_COUNT]; /* 0x60 */
	aword trust[PLAYER_MAX_COUNT]; /* 0x6C */
	aword broken_treaty[PLAYER_MAX_COUNT]; /* 0x78 */
	aword dip_blunder[PLAYER_MAX_COUNT]; /* 0x84 */
	aword tribute_tech_field[PLAYER_MAX_COUNT]; /* 0x90 */
	aword tribute_tech[PLAYER_MAX_COUNT]; /* 0x9C */
	aword mood_treaty[PLAYER_MAX_COUNT]; /* 0xA8 */
	aword mood_trade[PLAYER_MAX_COUNT]; /* 0xB4 */
	aword mood_tech_trade[PLAYER_MAX_COUNT]; /* 0xC0 */
	aword mood_peace_treaty[PLAYER_MAX_COUNT]; /* 0xCC */
	aword treaty[PLAYER_MAX_COUNT]; /* 0xD8 => 0:no,1:non-agg,2:alliance,
		3:war,4:finalwar */
	aword trade[PLAYER_MAX_COUNT]; /* 0xE4 => bc/year */
	/* 20x12=240@0xF0 */
	aword trade_pct[PLAYER_MAX_COUNT]; /* 0xF0 => 0-100 */
	aword ai_next_spy_mode[PLAYER_MAX_COUNT]; /* 0xFC => 0:hide,
		1:espionage,2:sabotage */
	aword unknown2[PLAYER_MAX_COUNT]; /* 0x108 */
	aword unknown3[PLAYER_MAX_COUNT]; /* 0x114 */
	aword unknown4[PLAYER_MAX_COUNT]; /* 0x120 */
	aword unknown5[PLAYER_MAX_COUNT]; /* 0x12C */
	aword audience_ttrade_num[PLAYER_MAX_COUNT]; /* 0x138 */
	/* 27x12=324@0x144 */
	aword audience_ttrade_field[PLAYER_MAX_COUNT][TECH_COUNT]; /* 0x144 */
	aword audience_ttrade_tech[PLAYER_MAX_COUNT][TECH_COUNT]; /* 0x18C */
	/* 39x12=468@0x1D4 */
	aword incentive_offer_tech_field[PLAYER_MAX_COUNT]; /* 0x1D4 */
	aword incentive_offer_tech[PLAYER_MAX_COUNT]; /* 0x1E0 */
	aword incentive_offer_bc[PLAYER_MAX_COUNT]; /* 0x1EC */
	aword audience_attack_of_ally[PLAYER_MAX_COUNT]; /* 0x1F8 */
	aword audience_ask_break_treaty[PLAYER_MAX_COUNT]; /* 0x204 */
	aword unknown6[PLAYER_MAX_COUNT]; /* 0x210 */
	aword hated_party[PLAYER_MAX_COUNT]; /* 0x21C */
	aword mutual_enemy[PLAYER_MAX_COUNT]; /* 0x228 */
	aword audience_attack_gift_tech_field[PLAYER_MAX_COUNT]; /* 0x234 */
	aword audience_attack_gift_tech[PLAYER_MAX_COUNT]; /* 0x240 */
	aword audience_attack_gift_bc[PLAYER_MAX_COUNT]; /* 0x24C */
	aword unknown7[PLAYER_MAX_COUNT]; /* 0x258 */
	aword unknown8[PLAYER_MAX_COUNT]; /* 0x264 */
	aword hatred[PLAYER_MAX_COUNT]; /* 0x270 */
	aword have_met[PLAYER_MAX_COUNT]; /* 0x27C => (0:no,1:just,2:introduced) */
	aword established_trade_bc[PLAYER_MAX_COUNT]; /* 0x288 */
	aword unknown9[PLAYER_MAX_COUNT]; /* 0x294 */
	/* 56x12=672@0x2A0 */
	aword have_planetary_shield_tech; /* 0x2A0 => (0,5,10,15,20) */
	aword unknown10; /* 0x2A2 */
	aword spying_pct_tenths[PLAYER_MAX_COUNT]; /* 0x2A4 */
	aword spy_fund_leftover[PLAYER_MAX_COUNT]; /* 0x2B0 */
	aword unknown11[PLAYER_MAX_COUNT]; /* 0x2BC */
	aword spy_mode[PLAYER_MAX_COUNT]; /* 0x2C8 => 0:hide,
		1:espionage,2:sabotage */
	aword security_pct_tenths; /* 0x2D4 => 0-10 */
	aword spies[PLAYER_MAX_COUNT]; /* 0x2D6 */
	aword trade_bc; /* 0x2E2 */
	aword ship_maintenance_bc; /* 0x2E4 */
	aword base_maintenance_bc; /* 0x2E6 */
	aword unknown13[2]; /* 0x2E8 */
	aword spy_maintenance_bc; /* 0x2EC */
	aword conv_total2actual_pct; /* 0x2EE */
	adwrd total_maintenance_bc; /* 0x2F0 => signed */
	adwrd total_research_bc; /* 0x2F4 */
	adwrd total_production_bc; /* 0x2F8 */
	adwrd reserve_bc; /* 0x2FC */
	aword tax_tenths; /* 0x300 => CHECKED! */
	aword best_base_shield; /* 0x302 */
	aword best_base_computer; /* 0x304 */
	aword best_base_weapon; /* 0x306 */
	aword have_sub_space_interdictor; /* 0x308 */
	aword antidote; /* 0x30A => (0,1,2) */
	aword have_colony_tech; /* 0x30C */
	aword have_eco_restoration; /* 0x30E => (2,3,5,10,20) */
	aword have_terraform; /* 0x310 */
	aword terraform_cost_per_inc; /* 0x312 */
	aword have_adv_soil_enrich; /* 0x314 */
	aword have_atmos_terraforming; /* 0x316 */
	aword have_soil_enrich; /* 0x318 */
	aword cost_pop_inc; /* 0x31A */
	aword unknown14; /* 0x31C */
	aword have_imp_adv_scanner; /* 0x31E */
	aword have_adv_scanner; /* 0x320 */
	aword have_hyperspace_comm; /* 0x322 */
	aword have_stargates; /* 0x324 */
	aword colonist_factories; /* 0x326 */
	aword unknown15[2]; /* 0x328 */
	aword industrial_waste_scale; /* 0x32C => (0,2,4,6,8,10) */
	aword fuelrange; /* 0x32E => (3-10,30) */
	aword have_combat_transporter; /* 0x330 */
}
empire1_data_t;

/* 0x6C (108) */
#define TECH_DATA_SIZE 108

typedef struct _tech_data_t
{
	aword tech_level[TECH_COUNT];
	aword tech_slider[TECH_COUNT]; /* percentage? */
	adwrd tech_research_invest[TECH_COUNT];
	aword tech_research_level[TECH_COUNT];
	adwrd tech_research_full_cost[TECH_COUNT];
	aword tech_completed_project[TECH_COUNT]; /* including level 1? */
	aword tech_slider_lock[TECH_COUNT];
}
tech_data_t;

/* 0x04 (4) */
#define EMPIRE2_DATA_SIZE 4

typedef struct _empire2_data_t
{
	aword have_engine;
	aword ship_design_count;
}
empire2_data_t;

/* 0x18 (24) */
#define FLEET_DATA_SIZE 0x18

typedef struct _fleet_data_t
{
	aword visible[PLAYER_MAX_COUNT];
	aword ship_count[SHIP_TYPE_COUNT];
}
fleet_data_t;

/* 0x12 (18) */
#define EMPIRE3_DATA_SIZE 18

typedef struct _empire3_data_t
{
	aword spy_report[TECH_COUNT];
	aword spy_report_year;
	aword ai_colony_ship_index;
	aword ai_bomber_ship_index;
}
empire3_data_t;

/* 0xDD4 (3540) */
#define PLAYER_DATA_SIZE 3540

typedef struct __attribute__ ((packed)) _player_t
{
	empire1_data_t empire1_data;
	tech_data_t tech_data;
	empire2_data_t empire2_data;
	fleet_data_t fleet_data[PLANET_MAX_COUNT];
	empire3_data_t empire3_data;
}
player_t;

/** ship design/research => full data */

/* 0x44 (68) */
#define SHIP_DESIGN_DATA_SIZE 68
#define SHIP_NAME_SIZE 12
#define SHIP_WEAPON_SLOT 4
#define SHIP_SPECIAL_SLOT 2

typedef struct _ship_design_t
{
	abyte ship_name[SHIP_NAME_SIZE];
	aword unknown1[4]; /* 0x0C */
	aword ship_cost; /* 0x14 */
	aword space; /* 0x16 */
	aword hull_size; /* 0x18 => (0-3) */
	aword look; /* 0x1A */
	aword weapon_type[SHIP_WEAPON_SLOT]; /* 0x1C => 0-63 */
	aword weapon_num[SHIP_WEAPON_SLOT]; /* 0x24 => 0-63 */
	aword engine; /* 0x2C => (0-8) */
	adwrd engine_count; /* 0x2E */
	aword special_type[SHIP_SPECIAL_SLOT]; /* 0x32 => (0-31) */
	aword shield; /* 0x38 => (0-11) */
	aword jammer; /* 0x3A => (0-10) */
	aword computer; /* 0x3C => (0-11) */
	aword armor; /* 0x3E => (0-13) */
	aword speed; /* 0x40 => (0-8) */
	aword hit_points; /* 0x42 */
}
ship_design_t;

/* 0x468 (1128) */
#define FULL_DATA_SIZE 1128
#define SHIP_NAMES_COUNT 12
#define TECH_QUADRANT 10

typedef struct _full_data_t
{
	/* size:0x44x6 (68x6) */
	ship_design_t ship_design[SHIP_TYPE_COUNT];
	/* size:0x90 (144) */
	abyte name[SHIP_NAMES_COUNT][SHIP_NAME_SIZE]; /* 0x198 */
	/* size:0x21C (540) */
	abyte research_limited[TECH_COUNT][TECH_QUADRANT][3]; /* 0x228 */
	abyte research_completed[TECH_COUNT][TECH_QUADRANT][6]; /* 0x2DC */
	/* the rest... */
	aword have_reserve_fuel[SHIP_TYPE_COUNT]; /* 0x444 */
	aword year_of_design[SHIP_TYPE_COUNT]; /* 0x450 */
	aword total_ship_count[SHIP_TYPE_COUNT]; /* 0x45C */
}
full_data_t;

/* 0x336 (822) */
#define GAME_EVENTS_DATA_SIZE 822
#define EVENTS_NEW_TECH_COUNT 15

typedef struct _game_events_t
{
	aword year_last_event; /* 0x00 */
	aword unused1; /* 0x02 => always 30? */
	aword happened[20]; /* 0x04 */
	aword have_plague; /* 0x2C => (0-3) */
	aword plague_player; /* 0x2E */
	aword plague_planet; /* 0x30 */
	aword plague_value; /* 0x32 */
	aword have_event02; /* 0x34 => (0-1) */
	aword event02_player; /* 0x36 */
	aword event02_planet; /* 0x38 */
	aword have_nova; /* 0x3A => (0-3) */
	aword nova_player; /* 0x3C */
	aword nova_planet; /* 0x3E */
	aword nova_years; /* 0x40 */
	aword nova_value; /* 0x42 */
	aword have_accident; /* 0x44 */
	aword accident_player; /* 0x46 */
	aword accident_planet; /* 0x48 */
	aword have_event05; /* 0x4A => (0-1) */
	aword event05_player2; /* 0x4C */
	aword event05_player; /* 0x4E */
	aword have_event06; /* 0x50 => (0-1) */
	aword event06_player; /* 0x52 */
	aword event06_tech_field; /* 0x54 */
	aword have_comet; /* 0x56 => (0-3) */
	aword comet_player; /* 0x58 */
	aword comet_planet; /* 0x5A */
	aword comet_years; /* 0x5C */
	aword comet_hit_points; /* 0x5E */
	aword comet_damage; /* 0x60 */
	aword have_pirates; /* 0x62 => (0-3) */
	aword pirates_what; /* 0x64 */
	aword pirates_planet; /* 0x66 */
	aword pirates_hit_points; /* 0x68 */
	aword have_event09; /* 0x6A => (0-1) */
	aword event09_player; /* 0x6C */
	aword have_crystal; /* 0x6E => (0-3) */
	aword crystal_x; /* 0x70 */
	aword crystal_y; /* 0x72 */
	aword crystal_counter; /* 0x74 */
	aword crystal_killer; /* 0x76 */
	aword crystal_destination; /* 0x78 */
	aword crystal_destroyed_planets; /* 0x7A */
	aword have_amoeba; /* 0x7C => (0-3) */
	aword amoeba_x; /* 0x7E */
	aword amoeba_y; /* 0x80 */
	aword amoeba_counter; /* 0x82 */
	aword amoeba_killer; /* 0x84 */
	aword amoeba_destination; /* 0x86 */
	aword amoeba_destroyed_planets; /* 0x88 */
	aword have_event13; /* 0x8A => (0-1) */
	aword event13_planet; /* 0x8C */
	aword have_event14; /* 0x8E => (0-1) */
	aword event14_planet; /* 0x90 */
	aword have_event15; /* 0x92 => (0-1) */
	aword event15_planet; /* 0x94 */
	aword have_event16; /* 0x96 => (0-1) */
	aword event16_planet; /* 0x98 */
	aword have_orion; /* 0x9A => (0-1) */
	aword orion_planet_index; /* 0x9C */
	aword have_guardian; /* 0x9E */
	aword home_planet_index[PLAYER_MAX_COUNT]; /* 0xA0 */
	aword report_star_colonization_status; /* 0xAC => 0-4 */
	aword unknown1[31]; /* 0xAE */
	aword coup_player; /* 0xEC => 0-5,-1 */
	aword new_tech_num; /* 0xEE */
	aword new_tech_field[EVENTS_NEW_TECH_COUNT]; /* 0xF0 */
	aword new_tech_tech_index[EVENTS_NEW_TECH_COUNT]; /* 0x10E */
	aword new_tech_source[EVENTS_NEW_TECH_COUNT]; /* 0x12C => 0-4 */
	aword new_tech_val1[EVENTS_NEW_TECH_COUNT]; /* 0x14A */
	aword new_tech_val2[EVENTS_NEW_TECH_COUNT]; /* 0x168 */
	aword new_tech_can_frame[EVENTS_NEW_TECH_COUNT]; /* 0x186 => 0-1 */
	aword new_ships[SHIP_TYPE_COUNT]; /* 0x1A4 */
	aword unknown2[33]; /* 0x1B0 */
	aword spies_caught_us[PLAYER_MAX_COUNT]; /* 0x1F2 */
	aword spies_caught_them[PLAYER_MAX_COUNT]; /* 0x1FE */
	aword spies_caught_planet[PLANET_MAX_COUNT]; /* 0x20A */
	abyte help_show[20]; /* 0x2E2 => 0-1 */
	aword build_finished_number; /* 0x2F6 => 0-20 */
	abyte build_finished_planet_index[20]; /* 0x2F8 */
	abyte build_finished_type[20]; /* 0x30C */
	aword voted_for[PLAYER_MAX_COUNT]; /* 0x320 */
	aword best_eco_restore; /* 0x32C */
	aword best_waste_reduce; /* 0x32E */
	aword best_something; /* 0x330 */
	aword best_robotic_control; /* 0x332 */
	aword best_terraform; /* 0x334 */
}
game_events_t;

/* 0x4E6 (1254) */
#define GAME_DATA_DATA_SIZE 1254
#define NAMES_DATA_SIZE 15
#define NEBULA_DIMENSION 4

typedef struct __attribute__ ((packed)) _game_data_t
{
	aword fleets_enroute; /* 0xE1B6:0x000 */
	aword transport_enroute; /* 0xE1B8:0x002 */
	abyte names[PLAYER_MAX_COUNT][NAMES_DATA_SIZE]; /* 0xE1BA:0x004 */
	aword unknown1[15]; /* 0xE214:0x05E */
	aword year; /* 0xE232:0x07C => 0-2299 */
	aword unused1; /* 0xE234:0x07E */
	aword selected_planet; /* 0xE236:0x080 */
	aword nebula_count; /* 0xE238:0x082 */
	aword nebula_type[NEBULA_DIMENSION]; /* 0xE23A:0x084 */
	aword nebula_x[NEBULA_DIMENSION]; /* 0xE242:0x08C */
	aword nebula_y[NEBULA_DIMENSION]; /* 0xE24A:0x094 */
	aword nebula_x0[NEBULA_DIMENSION][NEBULA_DIMENSION]; /* 0xE252:0x09C */
	aword nebula_x1[NEBULA_DIMENSION][NEBULA_DIMENSION]; /* 0xE272:0x0BC */
	aword nebula_y0[NEBULA_DIMENSION][NEBULA_DIMENSION]; /* 0xE292:0x0DC */
	aword nebula_y1[NEBULA_DIMENSION][NEBULA_DIMENSION]; /* 0xE2B2:0x0FC */
	aword players; /* 0xE2D2:0x11C */
	aword difficulty; /* 0xE2D4:0x11E => 0-4 */
	aword galaxy_stars; /* 0xE2D6:0x120 => planet count! */
	aword galaxy_size; /* 0xE2D8:0x122 => 0-3 */
	aword galaxy_width; /* 0xE2DA:0x124 => 6,8,a,c */
	aword galaxy_height; /* 0xE2DC:0x126 => 4,6,7,9 */
	aword galaxy_maxx; /* 0xE2DE:0x128 */
	aword galaxy_maxy; /* 0xE2E0:0x12A */
	aword seen_owner[PLANET_MAX_COUNT]; /* 0xE2E2:0x12C */
	aword seen_pop[PLANET_MAX_COUNT]; /* 0xE3BA:0x204 */
	aword seen_bases[PLANET_MAX_COUNT]; /* 0xE492:0x2DC */
	aword seen_factories[PLANET_MAX_COUNT]; /* 0xE56A:0x3B4 */
	ship_design_t current_design; /* 0xE642:0x48C */
	aword end; /* 0xE686:0x4D0 => (0:none,1:exile,2:won,3:finalwar) */
	aword winner; /* 0xE688:0x4D2 => (0-5) */
	aword guardian_killer; /* 0xE68A:0x4D4 */
	aword election_held; /* 0xE68C:0x4D6 */
	abyte have_new_ships[14]; /* 0xE68E:0x4D8 */
}
game_data_t;

/** __attribute__ ((packed))  */
#define ORION_DATA_SIZE 59036

typedef struct _orion_t
{
	planet_t planet[PLANET_MAX_COUNT];
	/* 0x4DA0 (19872) */
	fleet_enroute_t f_enroute[FLEET_ENROUTE_MAX_COUNT];
	/* 0x6A10 (27152) <= 0x1C70 (7280) */
	transport_enroute_t t_enroute[TRANSPORT_ENROUTE_MAX_COUNT];
	/* 0x7118 (28952) <= 0x708 (1800) */
	player_t player[PLAYER_MAX_COUNT];
	/* 0xC410 (50192) <= 0x52F8 (21240) */
	full_data_t fulldata[PLAYER_MAX_COUNT];
	/* 0xDE80 (56960) <= 0x1A70 (6768) */
	game_events_t game_event;
	/* 0xE1B6 (57782) <= 0x336 (822) */
	game_data_t game_data;
	/* 0xE69C (59036) <= 0x4E6 (1254) */
}
orion_t;

int show_planet(orion_t* orion,char* name)
{
	planet_t *planet = 0x0;
	int loop;
	for (loop=0;loop<orion->game_data.galaxy_stars;loop++)
	{
		planet_t *ptemp = &orion->planet[loop];
		if (!ptemp->name[0]||ptemp->planetary_index!=loop)
			break;
		if (!strcasecmp((char*)ptemp->name,name))
		{
			planet = ptemp;
			break;
		}
	}
	if (!planet)
	{
		printf("\nCannot find planet '%s'! Aborting!\n",name);
		return -1;
	}
	printf("***********\n");
	printf("PLANET INFO\n");
	printf("***********\n");
	printf("Planet: %s [%d](%d,%d)\n",planet->name,
		planet->planetary_index,planet->posx,planet->posy);
	printf("Type:%s, Growth:%s, Special:%s\n",
		planet->type==0?"Normal":planet->type==2?"Toxic":
		planet->type==3?"Inferno":planet->type==4?"Dead":
		planet->type==7?"Minimal":planet->type==8?"Desert":
		planet->type==9?"Steppe":planet->type==10?"Arid":
		planet->type==11?"Ocean":planet->type==13?"Terran":"Huh?",
		planet->growth==0?"Hostile":planet->growth==1?"Normal":
		planet->growth==2?"Fertile":"Huh?",
		planet->special==0?"Normal":planet->special==1?"Ultra Poor":
		planet->special==2?"Poor":planet->special==3?"Artifacts":
		planet->special==4?"Rich":planet->special==5?"Ultra Rich":
		planet->special==6?"4x Tech":"Huh?");
	printf("Star:%d, Look:%d, Frame:%d, HMM:%d, Rocks:%d\n",
		planet->star,planet->look,planet->frame,planet->what1,planet->rocks);
	printf("Population:%d (%d)(Prev:%d)\n",planet->population,
		planet->population_tenths,planet->population_prev);
	printf("MaxPop1:%d, MaxPop2:%d, MaxPop3:%d\n",
		planet->max_pop1,planet->max_pop2,planet->max_pop3);
	printf("Owner:%d (Prev:%d)\n",planet->owner,planet->owner_prev);
	printf("Factories:%d, Waste:%d\n",planet->factories,planet->waste);
	return loop;
}

void show_empire(orion_t* orion,int index)
{
	player_t *player;
	if (index<0||index>=orion->game_data.players)
	{
		printf("\nInvalid player index %d! Aborting!\n",index);
		return;
	}
	player = &orion->player[index];
	printf("***********\n");
	printf("EMPIRE INFO\n");
	printf("***********\n");
	printf("Emperor: %s, Race: %s (%d)\n",orion->game_data.names[index],
		player->empire1_data.race==0?"Human":
		player->empire1_data.race==1?"Mrrshan":
		player->empire1_data.race==2?"Silicoid":
		player->empire1_data.race==3?"Sakkra":
		player->empire1_data.race==4?"Psilon":
		player->empire1_data.race==5?"Alkari":
		player->empire1_data.race==6?"Klackon":
		player->empire1_data.race==7?"Bulrathi":
		player->empire1_data.race==8?"Meklar":
		player->empire1_data.race==9?"Darlok":"Unknown",
		player->empire1_data.race);
	printf("Reserve BC: %d\n",player->empire1_data.reserve_bc);
}

int load_game(orion_t* orion, char* filename)
{
	int tell = 0;
	FILE *pfile = fopen(filename,"rb+");
	if(!pfile)
	{
		printf("\nCannot load file '%s'!\n",filename);
		return -1;
	}
	/* get file size */
	fseek(pfile,0,SEEK_END);
	tell = ftell(pfile);
	/* read the whole file and close */
	fseek(pfile,0,SEEK_SET);
	fread((void*)orion,sizeof(orion_t),1,pfile);
	fclose(pfile);
	return tell;
}

int save_game(orion_t* orion, char* filename)
{
	FILE *pfile = fopen(filename,"rb+");
	if(!pfile)
	{
		printf("\nCannot save file '%s'!\n",filename);
		return -1;
	}
	fseek(pfile,0,SEEK_SET);
	fwrite((void*)orion,sizeof(orion_t),1,pfile);
	fclose(pfile);
	return 0;
}

#define ERROR_PARAM_SLOT -2
#define ERROR_PARAM_NAME -3
#define ERROR_INVALID_PARAM -4

int main(int argc, char *argv[])
{
	int slot = 1, loop, temp, task = TASK_LIST_GAMEDATA;
	int cheat = 0;
	char savegame[16], *pname = 0x0;
	orion_t odata;

	for(loop=1;loop<argc;loop++)
	{
		if(!strcmp(argv[loop],"--slot"))
		{
			loop++;
			if(loop<argc)
				slot = atoi(argv[loop]);
			else
			{
				printf("Cannot get slot parameter!\n");
				return ERROR_PARAM_SLOT;
			}
		}
		else if(!strcmp(argv[loop],"--planet"))
		{
			loop++;
			if(loop<argc)
			{
				pname = argv[loop];
				task = TASK_LIST_PLANET;
			}
			else
			{
				printf("Cannot get planet name parameter!\n");
				return ERROR_PARAM_NAME;
			}
		}
		else if(!strcmp(argv[loop],"--cheat"))
		{
			cheat = 1;
		}
		else
		{
			printf("Invalid parameter '%s'! Aborting!\n",argv[loop]);
			return ERROR_INVALID_PARAM;
		}
	}
	/* sanity check */
	if (sizeof(orion_t)!=ORION_DATA_SIZE)
	{
		printf("[DEBUG] OrionData Size: %d(%x)/%d(%x)\n",
			(int)sizeof(orion_t),(int)sizeof(orion_t),
			ORION_DATA_SIZE,ORION_DATA_SIZE);
	}
	if (sizeof(planet_t)!=PLANET_DATA_SIZE)
	{
		printf("[DEBUG] Planet Size: %d(%x)/%d(%x)\n",
			(int)sizeof(planet_t),(int)sizeof(planet_t),
			PLANET_DATA_SIZE,PLANET_DATA_SIZE);
	}
	if (sizeof(fleet_enroute_t)!=FLEET_ENROUTE_DATA_SIZE)
	{
		printf("[DEBUG] Fleet enroute Size: %d(%x)/%d(%x)\n",
			(int)sizeof(fleet_enroute_t),(int)sizeof(fleet_enroute_t),
			FLEET_ENROUTE_DATA_SIZE,FLEET_ENROUTE_DATA_SIZE);
	}
	if ((int)sizeof(transport_enroute_t)!=TRANSPORT_ENROUTE_DATA_SIZE)
	{
		printf("[DEBUG] Transport enroute Size: %d(%x)/%d(%x)\n",
			(int)sizeof(transport_enroute_t),(int)sizeof(transport_enroute_t),
			TRANSPORT_ENROUTE_DATA_SIZE,TRANSPORT_ENROUTE_DATA_SIZE);
	}
	if (sizeof(player_t)!=PLAYER_DATA_SIZE)
	{
		printf("[DEBUG] Player Size: %d(%x)/%d(%x)\n",
			(int)sizeof(player_t),(int)sizeof(player_t),
			PLAYER_DATA_SIZE,PLAYER_DATA_SIZE);
	}
	if (sizeof(empire1_data_t)!=EMPIRE1_DATA_SIZE)
	{
		printf("[DEBUG] Empire1 Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(empire1_data_t),(int)sizeof(empire1_data_t),
			EMPIRE1_DATA_SIZE,EMPIRE1_DATA_SIZE);
	}
	if (sizeof(empire2_data_t)!=EMPIRE2_DATA_SIZE)
	{
		printf("[DEBUG] Empire2 Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(empire2_data_t),(int)sizeof(empire2_data_t),
			EMPIRE2_DATA_SIZE,EMPIRE2_DATA_SIZE);
	}
	if (sizeof(empire3_data_t)!=EMPIRE3_DATA_SIZE)
	{
		printf("[DEBUG] Empire3 Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(empire3_data_t),(int)sizeof(empire3_data_t),
			EMPIRE3_DATA_SIZE,EMPIRE3_DATA_SIZE);
	}
	if (sizeof(tech_data_t)!=TECH_DATA_SIZE)
	{
		printf("[DEBUG] Tech Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(tech_data_t),(int)sizeof(tech_data_t),
			TECH_DATA_SIZE,TECH_DATA_SIZE);
	}
	if (sizeof(fleet_data_t)!=FLEET_DATA_SIZE)
	{
		printf("[DEBUG] Fleet Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(fleet_data_t),(int)sizeof(fleet_data_t),
			FLEET_DATA_SIZE,FLEET_DATA_SIZE);
	}
	if (sizeof(full_data_t)!=FULL_DATA_SIZE)
	{
		printf("[DEBUG] Full Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(full_data_t),(int)sizeof(full_data_t),
			FULL_DATA_SIZE,FULL_DATA_SIZE);
	}
	if (sizeof(game_events_t)!=GAME_EVENTS_DATA_SIZE)
	{
		printf("[DEBUG] Game Events Size: %d(%x)/%d(%x)\n",
			(int)sizeof(game_events_t),(int)sizeof(game_events_t),
			GAME_EVENTS_DATA_SIZE,GAME_EVENTS_DATA_SIZE);
	}
	if (sizeof(ship_design_t)!=SHIP_DESIGN_DATA_SIZE)
	{
		printf("[DEBUG] Ship Design Size: %d(%x)/%d(%x)\n",
			(int)sizeof(ship_design_t),(int)sizeof(ship_design_t),
			SHIP_DESIGN_DATA_SIZE,SHIP_DESIGN_DATA_SIZE);
	}
	if (sizeof(game_data_t)!=GAME_DATA_DATA_SIZE)
	{
		printf("[DEBUG] Game Data Size: %d(%x)/%d(%x)\n",
			(int)sizeof(game_data_t),(int)sizeof(game_data_t),
			GAME_DATA_DATA_SIZE,GAME_DATA_DATA_SIZE);
	}
	/* create the file name from slot parameter */
	sprintf(savegame,"SAVE%d.GAM",slot);
	/* load game file */
	temp = load_game(&odata,savegame);
	if (temp<0) exit(-1);
	/* show it! */
	printf("*********\n");
	printf("GAME INFO\n");
	printf("*********\n");
	printf("File: '%s', Size: %d bytes!\n",savegame,temp);
	/* must have correct file size! */
	if (temp!=ORION_DATA_SIZE)
	{
		printf("Invalid file size! Aborting!\n");
		exit(-1);
	}
	/* basic game info */
	printf("Game Year: %d,  Players: %d\n",
		odata.game_data.year+GAME_YEAR_INIT,
		odata.game_data.players);
	printf("Galaxy Size: %s (%d planets),  Difficulty: %s\n",
		odata.game_data.galaxy_size>2?"Huge":
		odata.game_data.galaxy_size>1?"Large":
		odata.game_data.galaxy_size>0?"Medium":"Small",
		odata.game_data.galaxy_stars,
		odata.game_data.difficulty>3?"Impossible":
		odata.game_data.difficulty>2?"Hard":
		odata.game_data.difficulty>1?"Average":
		odata.game_data.difficulty>0?"Easy":"Simple");
	printf("Fleet enroutes: %d, Transport enroutes: %d\n",
		odata.game_data.fleets_enroute,
		odata.game_data.transport_enroute);
	show_empire(&odata,0);
	temp = -1;
	if (task==TASK_LIST_PLANET)
	{
		temp = show_planet(&odata,pname);
	}
	/* cheat option */
	if (cheat)
	{
		printf("------\n");
		printf("CHEAT!\n");
		printf("------\n");
		odata.player[0].empire1_data.reserve_bc = 32000;
		printf("Funding '%s'! (Current:%d)\n",odata.game_data.names[0],
			odata.player[0].empire1_data.reserve_bc);
		if (temp>=0)
		{
			planet_t *ptemp = &odata.planet[temp];
			printf("Making planet '%s' fertile and 4xtech!\n",ptemp->name);
			odata.planet[temp].population = odata.planet[temp].max_pop1;
			odata.planet[temp].type = PLANET_TYPE_TERRAN;
			odata.planet[temp].growth = PLANET_GROWTH_FERTILE;
			odata.planet[temp].special = PLANET_SPECIAL_4XTECH;
		}
		save_game(&odata,savegame);
	}
	return 0;
}
